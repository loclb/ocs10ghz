from posixpath import dirname
import random
from math import floor
import glob, os
import tensorflow as tf
import numpy as np 
from Modules import readSpecFromCSV, MatrixNormalization
from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras import layers
from tensorflow.keras import models
import logging
import time 

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.INFO)
logging.info('Startting------------------------------------------')
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


# gpus = tf.config.list_physical_devices('GPU')
# print(gpus)


# PathNew = "/4T-media/loc/OCS_AI/Data/Train"

PathNew = "/4T-media/loc/OCS_AI/Data/Train-cap-4-shift-05"
print(PathNew)
print(os.listdir(PathNew))

training_list = []
val_list = []
test_list = []
scale_train = 60
scale_val = 20
scale_test = 100 - scale_train-scale_val
seed = 42
random.seed(seed)
label_list = os.listdir(PathNew)
print(label_list)
num_labels = len(label_list)
print(num_labels)
for Path_e in label_list:
    print(os.listdir(os.path.join(PathNew,Path_e)))
    
    listdir = os.listdir(os.path.join(PathNew,Path_e))
    listdir = [os.path.join(Path_e,dir) for dir in listdir]
    listdir = random.sample(listdir, len(listdir))
    print(listdir)
    number_of_sample = len(listdir)    
    train_num = floor(number_of_sample*scale_train/100)
    val_num = max(1,floor((number_of_sample*scale_val/100)))

    train_path = listdir[:train_num]
    val_path = listdir[train_num:train_num+val_num]
    test_path = listdir[train_num+val_num:]
    training_list.extend(train_path)
    val_list.extend(val_path)
    test_list.extend(test_path)

print("Training list:", training_list)
print("Val list:",val_list)
print("Test list :",test_list)

training_file_list =[]
val_file_list = []
test_file_list = []
for folder in training_list:
    listfile = glob.glob(os.path.join(PathNew,folder,"*.csv"))
    print(os.path.join(PathNew,folder,"*.csv"))
    print(len(listfile))
    training_file_list.extend(listfile)
print("------------------------------------")
for folder in val_list:
    listfile = glob.glob(os.path.join(PathNew,folder,"*.csv"))
    print(os.path.join(PathNew,folder,"*.csv"))
    print(len(listfile))
    val_file_list.extend(listfile)
print("------------------------------------")
for folder in test_list:
    listfile = glob.glob(os.path.join(PathNew,folder,"*.csv"))
    print(os.path.join(PathNew,folder,"*.csv"))
    print(len(listfile))
    test_file_list.extend(listfile)
print("------------------------------------")
print(len(training_file_list))
print(len(val_file_list))
print(len(test_file_list))

def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)
    return parts[-3]

def readSpecFromCSV_cc(filename):
    filename = bytes.decode(filename.numpy())
    # with open(filename, 'r') as f:
    #     lines = f.readlines()
    #     matrix_np = [[line.split(',')[:-1]] for line in lines]    
    # mt_np = np.array(matrix_np,dtype=np.float64)
    mt_np = np.loadtxt(filename,delimiter=',',dtype=np.float64)
    mt_np = MatrixNormalization(mt_np)
    
    return mt_np.squeeze()

def get_spectrogram_and_label_id(filename):
  spectrogram = readSpecFromCSV_cc(filename)
  spectrogram = spectrogram[:,:,np.newaxis]
  spectrogram = np.repeat(spectrogram,3,axis=2)
  spectrogram = tf.convert_to_tensor(spectrogram,dtype=tf.float64)
  label = get_label(filename)
  label_id = tf.argmax(label == label_list)
  return spectrogram, label_id

# AUTOTUNE = tf.data.AUTOTUNE
AUTOTUNE = tf.data.experimental.AUTOTUNE
print(tf.executing_eagerly())

def set_shapes(img, label):
    img.set_shape((513, 625, 3))
    label.set_shape([])
    return img, label
def preprocess_dataset(training_file_list):
    files_ds = tf.data.Dataset.from_tensor_slices(training_file_list)
    dataset_ds = files_ds.map(lambda x: tf.py_function(get_spectrogram_and_label_id, [x], \
        [tf.float64, tf.int64]) ,num_parallel_calls=AUTOTUNE)
    ds = dataset_ds.map(lambda img, label: set_shapes(img, label) ,num_parallel_calls=AUTOTUNE)
    return ds

train_ds = preprocess_dataset(training_file_list)
dataset_ds = train_ds
val_ds = preprocess_dataset(val_file_list)
test_ds = preprocess_dataset(test_file_list)

batch_size = 1
train_ds = train_ds.batch(batch_size)
val_ds = val_ds.batch(batch_size)

train_ds = train_ds.cache()
# .repeat()
# .prefetch(AUTOTUNE)
val_ds = val_ds.cache()
# .repeat()
# prefetch(AUTOTUNE)

for spectrogram, _ in dataset_ds.take(1):
  input_shape = spectrogram.shape
#   print(spectrogram)
print('Input shape:', input_shape)

# for spectrogram, _ in train_ds.take(1):
#   input_shape_1 = spectrogram.shape
# #   print(spectrogram)
# print('Input shape:', input_shape_1)

# base_model = tf.keras.applications.MobileNetV2(input_shape=(513, 625, 3),\
#                                                include_top=False,\
#                                                weights='imagenet')
base_model = tf.keras.applications.ResNet50(input_shape=(513, 625, 3),
                                               include_top=False,
                                               weights='imagenet')  

# add new classifier layers
flat1 = tf.keras.layers.Flatten()(base_model.layers[-1].output)
class1 = tf.keras.layers.Dense(256, activation='relu')(flat1)
# output = tf.keras.layers.Dense(2, activation='softmax')(class1)
# output = tf.keras.layers.Dense(2, activation='sigmoid')(class1)
output = tf.keras.layers.Dense(2)(class1)
# define new model
model = tf.keras.Model(inputs=base_model.inputs, outputs=output)
model.summary()

# model = models.Sequential()
# model.add(layers.Input(shape=input_shape))
# model.add(layers.Conv2D(32, kernel_size=(3, 3), activation='relu'))
# model.add(layers.Conv2D(64, kernel_size=(3, 3), activation='relu'))
# model.add(layers.MaxPooling2D())
# model.add(layers.Dropout(0.25))
# model.add(layers.Flatten())
# model.add(layers.Dense(128, activation='relu'))
# model.add(layers.Dropout(0.5))
# model.add(layers.Dense(num_labels))
# model.build((None, 513, 625, 1))
# model.summary()
print("MOdel:", model)

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.MeanSquaredError(),
    metrics=['accuracy'],
)
# model.compile(
#     optimizer=tf.keras.optimizers.Adam(),
#     loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
#     metrics=['sparse_categorical_accuracy'],
# )

# model.compile(
#     optimizer=tf.keras.optimizers.Adam(),
#     loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
#     metrics=['accuracy'],
# )

logging.info("Start training ------------------------------------------------")
start_time = time.time()
EPOCHS = 50
history = model.fit(
    train_ds, 
    validation_data=val_ds,  
    epochs=EPOCHS,
    callbacks=tf.keras.callbacks.EarlyStopping(verbose=1, patience=5),
)
end_time = time.time()
logging.info("Finish training -----------------------------------------------")

total_time_training = end_time - start_time
logging.info("Total training: %s m - %s s",total_time_training // 60, total_time_training % 60)

print('Training complete in {:.0f}m {:.0f}s'.format(
        total_time_training // 60, total_time_training % 60))

# model_save_name = "Test_model_Normalization_mse" +os.path.basename(PathNew)+"_" + str(EPOCHS) + ".h5"
# model_save_name = "Test_model_Mobinetv2_FineTune_Normalizaion_mse_softmax" + os.path.basename(PathNew) + str(EPOCHS) + ".h5"
model_save_name = "Test_model_Resnet50_FineTune_mse" + str(EPOCHS) + ".h5"
model.save(model_save_name)
logging.info("save file: %s",model_save_name)
# print("save file: ", model_save_name)


test_ds = test_ds.batch(batch_size)
model.evaluate(test_ds)




print("finished!")
