import numpy as np 
from scipy import signal
## Parameter 
Fs = 1000
time_win = 256
time_overlap = 250
N_fft = 1024

# Hann win
gwin = signal.windows.hann(time_win)

frequency_vector = np.linspace(0,Fs/2-Fs/N_fft,num=N_fft//2,endpoint=True)

# Filter 
a_filter = np.array([ 1.0000, -0.95])
b_filter = np.array([0.98,   -0.98])
