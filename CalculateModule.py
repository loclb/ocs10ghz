from re import T
import numpy as np
from math import floor
from scipy import signal,fft

def NumSegCal(dataLength,time_win,time_overlap):
    return floor((dataLength-time_overlap)/(time_win-time_overlap))

def TimeCal(dataLength,time_win,time_overlap):
    numSeg = NumSegCal(dataLength,time_win,time_overlap)
    return np.linspace(time_win/2,dataLength-time_win/2,num=numSeg,endpoint=True)
def StftCal(data,gwin,time_win,time_overlap,N_fft,log=True,Fs=None):
    numSeg = NumSegCal(len(data),time_win,time_overlap)
    assert len(gwin) == time_win, "gwin length must be equal time_win"
    STFT_matrix = np.zeros([N_fft//2,numSeg])

    for i in range(numSeg):
        j = i*(time_win - time_overlap)
        data_process = data[j:j+time_win].squeeze()
        data_process_fft = fft.fft(data_process*gwin,n=N_fft)
        data_process_fft_abs = np.abs(data_process_fft[0:N_fft//2])
        if log:

            STFT_matrix[:,i] = 20*np.log10(data_process_fft_abs)
        else:
            STFT_matrix[:,i] = data_process_fft_abs
    
    # t_vector = np.linspace(0,len(data),endpoint=False,num=len(data))
    t_vector = np.linspace(time_win/2,len(data)-time_win/2,num=numSeg,endpoint=True)
    f_vector = np.linspace(0,1/2-1/N_fft,num=N_fft//2,endpoint=True)
    if Fs is not None:
        t_vector = t_vector/Fs
        f_vector = f_vector*Fs
    return STFT_matrix, t_vector, f_vector