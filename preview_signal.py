# import matplotlib
# from numpy.core.fromnumeric import shape
# matplotlib.use('Agg')
import os
from numpy.core.fromnumeric import shape

from scipy.signal.spectral import stft
os.environ[ 'MPLCONFIGDIR' ] = '/4T-media/loc/tmp/'
import matplotlib.pyplot as plt
# plt.switch_backend('wxAgg')
# plt.switch_backend('TkAgg') 
# plt.switch_backend('QT4Agg')
from scipy.signal import lfilter,stft, windows
from scipy.fft import fft

import argparse
from tkinter import filedialog as fd
import tensorflow as tf 
import scipy as sp 
import pandas as pd
import numpy as np 
from math import floor


# file_name = fd.askopenfilename()
# print(file_name)
file_name = "/4T-media/loc/OCS_AI/Data/train/slowWalking/1571472491289_Minh_SlowWalking_2019-10-19---15-08-11.csv"
# parser = argparse.ArgumentParser()

vector = pd.read_csv(file_name,header=None).to_numpy()
low_channel = vector[:,0]
high_channel = vector[:,1]
mix_channel = vector[:,2]
print(mix_channel.shape)



## Parameter 
Fs = 1000
time_win = 1024
time_overlap = 1000
N_fft = 1024
# Filter 
a = np.array([ 1.0000, -0.95])
b = np.array([0.98,   -0.98])

# Hann win
gwin = windows.hann(time_win)
print("gwin:",gwin.shape)
# Axis
# endpoint_bo
# (len(mix_channel)-time_overlap)/(time_win - time_overlap)
numseg = floor((len(mix_channel)-time_overlap)/(time_win - time_overlap))
time_vector = np.linspace(time_win/2,len(mix_channel)-floor(time_win/2),\
    num=numseg,endpoint=True,retstep=False)*1/Fs
working_frequency = np.linspace(0,Fs/2-Fs/N_fft,num=int(N_fft/2),endpoint=True,retstep=False)
print(time_vector.shape)
print(working_frequency.shape)
time = np.linspace(0,len(mix_channel),num=len(mix_channel),endpoint=False)
time = time*1/Fs
print(time)
STFT_matrix = np.zeros([int(N_fft/2),numseg])
filter_signal = lfilter(b,a,mix_channel)
print(filter_signal)
print(vector.shape)
# vector 
model_save_folder = 'model_save'
model_name = "Normalization_mse_0_200FreTrain_50_1_channel_val_accuracy_modelCheckPoint.h5"
file_model = os.path.join(model_save_folder,model_name)

f, t, Sxx = stft(filter_signal, Fs, return_onesided=True,nfft=N_fft,\
    window='hann',nperseg=time_win,noverlap=time_overlap,\
        boundary=None,padded=False)

Sxx_abs = np.abs(Sxx)
Sxx_log = 20*np.log10(Sxx_abs)
for i in range(numseg):
    # 234744
    
    j = i*(time_win - time_overlap)
    # print(j)
    data_process = filter_signal[j:j+time_win].squeeze()
    # print("data_process:",data_process.shape)
    data_process_fft = fft(data_process*gwin)
    data_process_fft_mag =np.abs(data_process_fft[0:int(N_fft/2)])**2/np.sqrt(gwin.sum()**2)
    time_process = time[j:j+time_win]
    data_process_fft_powerlog = np.log10(data_process_fft_mag)
    STFT_matrix[:,i] =data_process_fft_powerlog

print("Sxx_abs:",Sxx_abs[0:10,0],Sxx_abs.shape)
print("Sxx_abs:",Sxx_abs[-10:-1,100],Sxx_abs.shape)
print("f:",f[0:10])
print("t:",t[0:10])
print("STFT_matrix:",STFT_matrix[0:10,0])
print("STFT_matrix:",STFT_matrix[-10:-1,100])
print("working_frequency:",working_frequency[0:10])
print("time_vector:",time_vector[0:10])
print("SFTF_matrix:",STFT_matrix.shape)
figure, ax = plt.subplots(2,1)
print(ax.shape)
ax[0].plot(time,mix_channel)
ax[0].set_xlim(0,np.max(time))
# ax[1].plot(time,filter_signal)
extend_imshow = [np.min(time_vector), np.max(time_vector),\
    np.min(working_frequency),np.max(working_frequency)]
# ax[1].imshow(STFT_matrix)
# ax[1].pcolormesh(time_vector,working_frequency,STFT_matrix)
# ax[1].set_xlim(np.min(time_vector), np.max(time_vector))
# ax[1].set_ylim(np.min(working_frequency),np.max(working_frequency))

# , ,  
# print(f.shape,t.shape,Sxx_log.shape)
# print(t[-1],time_vector[-1],time[-1],time_process[-1],j)
ax[1].pcolormesh(t,f,Sxx_log)
# ax[1].plot(time)
# mng = plt.get_current_fig_manager()
# _tkinter.TclError: bad argument "zoomed": must be normal, iconic, or withdrawn
# mng.full_screen_toggle()
# mng.window.state('withdrawn') 
# mng.frame.Maximize(True)
print(plt.get_backend())
# figure.savefig('fig.png')
plt.show()

