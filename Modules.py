import numpy as np
from numpy.core.defchararray import index 

# from math import rou   
def readSpecFromCSV(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        matrix_np = [[line.split(',')[:-1]] for line in lines]
    
    mt_np = np.array(matrix_np,dtype=np.float64)
    
    return mt_np.squeeze()

##  function
# function [index_output]=GetTimeIndexFromSTFT(time_input,time_win,time_overlap,Fs)

# haft_win = time_win/2;
# if time_input*Fs <= haft_win
#     index_output = 1;
# else    
#     index_output = ...
#         round((time_input*Fs-haft_win)/(time_win-time_overlap))+1;
# end
# end
def GetTimeIndexFromSTFT(time_input,time_win,time_overlap,Fs):
    half_win = time_win/2
    if time_input*Fs <= half_win:
        index_output = 0
    else:
        index_output = round((time_input*Fs - half_win)/(time_win - time_overlap))

    return index_output

# function [index_output]=GetTimeIndexFromSamplingFre(time_input,Fs, varargin)
#  Assume that the first index is 0 second
# if nargin < 3 
#     first_index_time = 0;
# else
#     first_index_time = varargin{1};
# end

# index_output = round((time_input - first_index_time)*Fs)+1;
# end
def GetTimeIndexFromSamplingFre(time_input,Fs, first_index_time=0):
    return round((time_input - first_index_time)*Fs)

# function [norm_matrix] = MatrixNormalization(matrix)

# mean_matrix = mean(matrix(:));
# std_matrix = std(matrix(:));
# norm_matrix = (matrix - mean_matrix)/std_matrix;

# end
def MatrixNormalization(matrix):
    mean_matrix = np.mean(matrix)
    std_matrix = np.std(matrix)
    return (matrix-mean_matrix)/std_matrix

def MatrixNormalization01(matrix):
    max_matrix = np.max(matrix)
    min_matrix = np.min(matrix)
    return (matrix-min_matrix)/(max_matrix-min_matrix)