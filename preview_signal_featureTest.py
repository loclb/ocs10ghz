import os
from tkinter.constants import S
from numpy.core.arrayprint import TimedeltaFormat 
import tensorflow as tf 
from scipy import fft, signal
import pandas as pd 
import numpy as np
from math import floor
from tkinter import filedialog as fd
import time

from Parameter import time_win, time_overlap, Fs,N_fft,\
    a_filter,b_filter, frequency_vector,gwin
import matplotlib.pylab as plt
from CalculateModule import NumSegCal, StftCal,TimeCal
from Modules import MatrixNormalization

file_name = fd.askopenfilename()
print(file_name)

vector = pd.read_csv(file_name,header=None).to_numpy()
low_channel = vector[:,0]
high_channel = vector[:,1]
mix_channel = vector[:,2]

# Select channel 
channel = 3
if channel == 1:
    print("Select Low Channel!")
    data = low_channel
elif channel == 2:
    print("Select High Channel!")
    data = high_channel
elif channel == 3:
    print("Select Mix Channel!")
    data = mix_channel
else:
    print("Select Mix Channel!")
    data = mix_channel

data = signal.lfilter(b_filter,a_filter,data)

time_vector = np.linspace(0,len(data)-1,num=len(data),\
    endpoint=True)/Fs

numseg = NumSegCal(len(data),time_win,time_overlap)
time_stft = TimeCal(len(data),time_win,time_overlap)

for i in range(numseg):
    j = i*(time_win - time_overlap)
    data_process = data[j:j+time_win].squeeze()
    data_process_fft = fft.fft(data_process*gwin,n=N_fft)
    data_process_fft_abs = fft.fft
