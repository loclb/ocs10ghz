import os
from tkinter.constants import S
from numpy.core.arrayprint import TimedeltaFormat 
import tensorflow as tf 
from scipy import signal
import pandas as pd 
import numpy as np
from math import floor
from tkinter import filedialog as fd
import time

import matplotlib.pylab as plt
from tensorflow.keras import models
from tensorflow.python.keras.saving.saving_utils import model_input_signature
from CalculateModule import NumSegCal, StftCal,TimeCal
from Modules import MatrixNormalization

# file_name = fd.askopenfilename()
# print(file_name)
# file_name = "/4T-media/loc/OCS_AI/Data/train/slowWalking/1571472491289_Minh_SlowWalking_2019-10-19---15-08-11.csv"
file_name = "/4T-media/loc/OCS_AI/Data/train/slowWalking/1570690954615_SlowWalking_2019-10-10---14-02-34.csv"

vector = pd.read_csv(file_name,header=None).to_numpy()
low_channel = vector[:,0]
high_channel = vector[:,1]
mix_channel = vector[:,2]

model_save_folder = 'model_save'
model_name = "Normalization_mse_0_200FreTrain_50_1_channel_val_accuracy_modelCheckPoint.h5"
# model_name = "Normalization_mse_0_200FreTrain_501_channel.h5"
# model_name = "Normalization_mse_0_200FreTrain_150_1_channel_val_accuracy_modelCheckPoint_1.h5"
file_model = os.path.join(model_save_folder,model_name)
model = tf.keras.models.load_model(file_model)
model.summary()

channel = 3
if channel == 1:
    print("Select Low Channel!")
    data = low_channel
elif channel == 2:
    print("Select High Channel!")
    data = high_channel
elif channel == 3:
    print("Select Mix Channel!")
    data = mix_channel
else:
    print("Select Mix Channel!")
    data = mix_channel



## Parameter 
Fs = 1000
time_win = 256
time_overlap = 250
N_fft = 1024

# Filter 
a = np.array([ 1.0000, -0.95])
b = np.array([0.98,   -0.98])
data = signal.lfilter(b,a,data)
time_vector = np.linspace(0,len(data)-1,num=len(data),endpoint=True)/Fs


# Hann win
gwin = signal.windows.hann(time_win)

# frame
frame_time = 4 # second
frame_overlap = 3.5 # second
frame_time = frame_time*Fs

frame_overlap = frame_overlap*Fs
print(frame_time,frame_overlap)
numFrame = NumSegCal(len(data),frame_time,frame_overlap)
numWinPerFrame = NumSegCal(frame_time,time_win,time_overlap)
# output_model = np.zeros(numFrame)
output = np.zeros(numFrame)
output_1 = np.zeros(numFrame)
output_2 = np.zeros(numFrame)
for i in range(numFrame):
    j = int(i*(frame_time - frame_overlap))
    # print(j)
    frame_process = data[j:j+int(frame_time)]
    start = time.time()
    Stft_frame, t_frame, f_frame = StftCal(frame_process,gwin,time_win,\
        time_overlap,N_fft,log=False,Fs=Fs)
    # print(Stft_frame.shape,t_frame.shape,f_frame.shape)    
    
    # f_frame, t_frame, Stft_frame = signal.stft(frame_process, Fs, return_onesided=True,nfft=N_fft,\
    # window='hann',nperseg=time_win,noverlap=time_overlap,\
    #     boundary=None,padded=False)
    end_time = time.time() - start
    # print(end_time)
    Stft_frame = np.abs(Stft_frame)
    # print(Stft_frame.shape)
    Stft_frame_norm = MatrixNormalization(Stft_frame)
    model_input =Stft_frame_norm[np.newaxis,0:200,:,np.newaxis]
    # print(Stft_frame_norm.shape)
    output_model = model(model_input).numpy().squeeze()
    # print(output_model)
    # print(output_model.shape)
    output_1[i] = output_model[0]
    output_2[i] = output_model[1]
    output[i] = np.argmax(output_model)

STFT_full, t_full, f_full = StftCal(data,gwin,time_win,time_overlap,N_fft,log=True,Fs=Fs)
fig , ax = plt.subplots(4,1)
fig.suptitle(os.path.basename(file_name))
ax[0].plot(time_vector,data)
ax[0].set_xlim(np.min(time_vector), np.max(time_vector))
ax[0].set_title("Time Domain")

ax[1].pcolormesh(t_full,f_full,STFT_full)
ax[1].set_xlim(np.min(time_vector), np.max(time_vector))
ax[1].set_title("Frequency Domain")
timeFrame = TimeCal(len(data),frame_time,frame_overlap)/Fs
ax[2].plot(timeFrame,output)
ax[2].set_xlim(np.min(time_vector), np.max(time_vector))
ax[2].set_ylim(-1, 2)
ax[2].set_title("Decision")
line, = ax[3].plot(timeFrame,output_1)
line.set_label(['Output 1'])
line2 ,= ax[3].plot(timeFrame,output_2)
line2.set_label(['Output 2'])
ax[3].legend()
ax[3].set_xlim(np.min(time_vector), np.max(time_vector))
ax[3].set_title("Outputs")

plt.subplots_adjust(hspace=0.3)
# ax[2].set_xlim(np.min(time_vector), np.max(time_vector))
file_save = os.path.basename(file_name)+".png"

fig.set_size_inches((9, 9), forward=False)
fig.savefig(file_save,dpi=400)
